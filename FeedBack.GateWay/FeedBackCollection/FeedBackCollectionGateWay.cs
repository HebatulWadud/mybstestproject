﻿
using FeedBack.Entity;
using FeedBack.GateWay.Connection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedBack.GateWay.FeedBackCollection
{
    public class FeedBackCollectionGateWay : ConnectionGateWay
    {
        #region  Posts
        public List<Post> GetPosts()
        {
            try
            {
                Query = "SELECT * from tbl_Post";
                Connection.Open();
                Command.CommandText = Query;
                Reader = Command.ExecuteReader();

                List<Post> posts = new List<Post>();
                while (Reader.Read())
                {
                    Post post = new Post()
                    {
                        PostId = (int)Reader["PostId"],
                        PostMessage = Reader["PostMessage"].ToString(),
                        CreateDate = Convert.ToDateTime(Reader["CreateDate"]),
                    };

                    posts.Add(post);

                }

                Reader.Close();
                return posts;
            }
            finally
            {
                if (Connection != null && Connection.State != ConnectionState.Closed)
                {
                    Connection.Close();
                }
            }
        }


        public List<Post> GetPostsWithDetails()
        {
            try
            {
                Query = "select COUNT(CASE WHEN fb.Interest = 0 THEN 1 END) as No, COUNT(CASE WHEN fb.Interest = 1 THEN 1 END) as Yes, fb.CommentId, c.CommentMsg, c.CreateDate as Commenttime, u.UserName, c.UserId, p.PostId, p.PostMessage, p.CreateDate as PostTime, p.UserId, up.UserName as PostUser from tbl_FeedBack fb left join tbl_Comment c on fb.CommentId =c.CommentId left join tbl_User u on c.UserId = u.UserId left join tbl_Post p on c.PostId = p.PostId left join tbl_User up on p.UserId = up.UserId group by fb.CommentId, c.CommentMsg, c.CreateDate, u.UserName,c.UserId,p.PostId, p.PostMessage, p.CreateDate,p.UserId, up.UserName";
                Connection.Open();
                Command.CommandText = Query;
                Reader = Command.ExecuteReader();

                List<Post> posts = new List<Post>();
                while (Reader.Read())
                {
                    Post post = new Post()
                    {
                        PostId = (int)Reader["PostId"],
                        PostMessage = Reader["PostMessage"].ToString(),
                        CreateDate = Convert.ToDateTime(Reader["PostTime"]),
                        UserId = (int)Reader["UserId"],
                        UserName = Reader["PostUser"].ToString(),
                        Comments = new List<Comments>(),
                    };

                    Comments comment = new Comments()
                    {
                        CommentId = (int)Reader["CommentId"],
                        CommentMsg = Reader["CommentMsg"].ToString(),
                        CreateDate = Convert.ToDateTime(Reader["Commenttime"]),
                        UserId = (int)Reader["UserId"],
                        UserName = Reader["UserName"].ToString(),
                        YesCount = (int)Reader["Yes"],
                        NoCount = (int)Reader["No"]
                    };

                    post.Comments.Add(comment);
                    posts.Add(post);

                }

                Reader.Close();
                return posts;
            }
            finally
            {
                if (Connection != null && Connection.State != ConnectionState.Closed)
                {
                    Connection.Close();
                }
            }
        }


        #endregion

        #region  Comment
        public int AddComment(Comments cm)
        {
            DateTime Currenttime = DateTime.UtcNow;
            try
            {
                Query = "Insert into tbl_Comment (PostId,UserId,CommentMsg,CreateDate) values ('" + cm.PostId + "','" + cm.UserId + "',N'" + cm.CommentMsg + "','" + Currenttime + "') ";
                Command.CommandText = Query;
                Connection.Open();
                int rowAffected = Command.ExecuteNonQuery();
                return rowAffected;
            }
            finally
            {
                if (Connection != null && Connection.State != ConnectionState.Closed)
                {
                    Connection.Close();
                }
            }
        }

        public List<Comments> GetCommentsByPosts(int PostId)
        {
            try
            {
                Query = "select c.CommentMsg, c.CommentId, c.UserId, u.UserName, c.PostId , p.PostMessage, c.CreateDate from tbl_Comment c  " +
                    "left join tbl_User u on c.UserId = u.UserId left join tbl_Post p on c.PostId = p.PostId where c.PostId = @PostId";
                Command.CommandText = Query;
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("PostId", PostId);
                Connection.Open();
                Reader = Command.ExecuteReader();

                List<Comments> posts = new List<Comments>();
                while (Reader.Read())
                {
                    Comments post = new Comments()
                    {
                        PostId = (int)Reader["PostId"],
                        UserId = (int)Reader["UserId"],
                        CommentId = (int)Reader["CommentId"],
                        CommentMsg = Reader["CommentMsg"].ToString(),
                        CreateDate = Convert.ToDateTime(Reader["CreateDate"]),
                        UserName = Reader["UserName"].ToString(),
                        PostMessage = Reader["PostMessage"].ToString()
                    };

                    posts.Add(post);

                }

                Reader.Close();
                return posts;
            }
            finally
            {
                if (Connection != null && Connection.State != ConnectionState.Closed)
                {
                    Connection.Close();
                }
            }
        }

        #endregion

        #region FeedBack

        public int AddFeedBack(FeedBackCount feedBack)
        {
            DateTime Currenttime = DateTime.UtcNow;
            try
            {
                Query = "Insert into tbl_FeedBack (PostId,UserId,CommentId,Interest,CreateDate) values ('" + feedBack.PostId + "','" + feedBack.UserId + "','" + feedBack.CommentId + "','" + feedBack.Interest + "','" + Currenttime + "') ";
                Command.CommandText = Query;
                Connection.Open();
                int rowAffected = Command.ExecuteNonQuery();
                return rowAffected;
            }
            finally
            {
                if (Connection != null && Connection.State != ConnectionState.Closed)
                {
                    Connection.Close();
                }
            }
        }




        #endregion



    }
}
