﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedBack.Entity
{
  public  class Comments
    {
        public int CommentId { get; set; }
        public int PostId { get; set; }
        public int UserId { get; set; }
        public string CommentMsg { get; set; }
        public DateTime CreateDate { get; set; }
        public string PostMessage { get; set; }
        public string UserName { get; set; }
        public int YesCount { get; set; }
        public int NoCount { get; set; }
    }
}
