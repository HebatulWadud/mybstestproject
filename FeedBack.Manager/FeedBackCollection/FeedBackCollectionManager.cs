﻿using FeedBack.Entity;
using FeedBack.GateWay.FeedBackCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedBack.Manager.FeedBackCollection
{
   public class FeedBackCollectionManager
    {
        FeedBackCollectionGateWay feedBackCollectionGateWay = new FeedBackCollectionGateWay();

        #region  Posts
        public List<Post> GetPosts()
        {
            return feedBackCollectionGateWay.GetPosts();
        }


        public List<Post> GetPostsWithDetails()
        {
            return feedBackCollectionGateWay.GetPostsWithDetails();
        }
        #endregion

        #region  Comments
        public int AddComment(Comments cm)
        {
            return feedBackCollectionGateWay.AddComment(cm);
        }


        public List<Comments> GetCommentsByPosts( int PostId)
        {
            return feedBackCollectionGateWay.GetCommentsByPosts(PostId);
        }

        #endregion



        #region FeedBack
        public int AddFeedBack(FeedBackCount feedBack)
        {
            return feedBackCollectionGateWay.AddFeedBack(feedBack);
        }
        #endregion
    }
}
