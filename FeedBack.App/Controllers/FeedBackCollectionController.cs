﻿using FeedBack.Entity;
using FeedBack.Manager.FeedBackCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Net.Http;

namespace FeedBackCollection.Controllers
{
    public class FeedBackCollectionController : Controller
    {
        FeedBackCollectionManager feedBackCollectionManager = new FeedBackCollectionManager();
        public ActionResult FeedBackCollectionDetails(int? page, string Search_Data, string Filter_Value)
        {
            

            IEnumerable<Post> posts = null;

            if (Search_Data != null)
            {
                page = 1;
            }
            else
            {
                Search_Data = Filter_Value;
            }

            ViewBag.FilterValue = Search_Data;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44303/api/");
                //HTTP GET
                var responseTask = client.GetAsync("feedbackApi");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Post>>();
                    readTask.Wait();

                    posts = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    posts = Enumerable.Empty<Post>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }


            if (!String.IsNullOrEmpty(Search_Data))
            {
                posts = posts.Where(s => s.PostMessage.ToUpper().Contains(Search_Data.ToUpper()));
            }
           
            int pageNumber = (page ?? 1);
            int pageSize = 15;

            return View(posts.ToPagedList(pageNumber, pageSize));
        }
    }
}